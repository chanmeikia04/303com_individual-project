package com.example.a303_project

import android.content.Context
import com.example.a303_project.ui.exhibition.ExhibitionDetailPresenter
import com.example.a303_project.ui.exhibition.ExhibitionModel
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class exhibitionDetailPresenterTest {
    lateinit var exhibitionDetailPresenter: ExhibitionDetailPresenter

    @RelaxedMockK
    lateinit var exhibitionModel: ExhibitionModel

    @RelaxedMockK
    lateinit var view: ExhibitionDetailPresenter.View

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp(){
        MockKAnnotations.init(this)

        exhibitionDetailPresenter = ExhibitionDetailPresenter(view, exhibitionModel)
    }

    @After
    fun tearDown(){
        clearAllMocks()
    }

    @Test
    fun testDisplayData(){
        //Given

        //When
        exhibitionDetailPresenter.displayData()

        //Then
        verify {
            //Name
            view.setName_cn(exhibitionModel.name_cn)
            view.setName_en(exhibitionModel.name_en)

            //Date
            if (exhibitionModel.getDate() !== "") {
                view.setDate(exhibitionModel.getDate())
            } else {
                view.hiddenDate()
            }

            //Type
            if (exhibitionModel.getSpecialExhibition()) {
                view.setType("專題展覽\nSpecial Exhibition")
            } else {
                view.setType("常設展覽\nPermanent Exhibition")
            }

            //Introduction
            view.setIntro_cn(exhibitionModel.getIntro_cn())
            view.setIntro_en(exhibitionModel.getIntro_en())

            if (exhibitionModel.getPic_url() !== "") {
                view.setPic(exhibitionModel.getPic_url())
//            Glide.with(viewHolder.itemView.getContext()).load(localDataSet.get(position).getPic_url()).into(viewHolder.getIvPic());
            }

            if (exhibitionModel.getWebsite_url() !== "") {
                view.setWebsite(exhibitionModel.getWebsite_url())
            }
        }
    }
}