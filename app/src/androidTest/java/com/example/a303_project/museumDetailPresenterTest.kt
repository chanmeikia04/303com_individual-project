package com.example.a303_project

import android.content.Context
import com.example.a303_project.ui.Museum.MuseumDetailPresenter
import com.example.a303_project.ui.Museum.MuseumModel
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class museumDetailPresenterTest {
    lateinit var museumDetailPresenter: MuseumDetailPresenter

    @RelaxedMockK
    lateinit var museumModel:MuseumModel

    @RelaxedMockK
    lateinit var view: MuseumDetailPresenter.View

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp(){
        MockKAnnotations.init(this)

        museumDetailPresenter = MuseumDetailPresenter(view, museumModel)
    }

    @After
    fun tearDown(){
        clearAllMocks()
    }

    @Test
    fun testDisplayData(){
        //Given

        //When
        museumDetailPresenter.displayData()

        //Then
        verify {
            //Name
            view.setName_cn(museumModel.name_cn)
            view.setName_en(museumModel.name_en)

            //Opening Hours
            if (museumModel.openingHours !== "") view.setHour(museumModel.openingHours) else view.setHour("Unprovided")

            //Tel
            view.setTel(museumModel.telephone)

            //Website
            view.setWebsite(museumModel.website)
            view.setWebsiteLink(museumModel.website)

            //Address
            view.setAddress_cn(museumModel.address_cn)
            view.setAddress_en(museumModel.address_en)

            //Remark
            if (museumModel.remark_cn !== "") view.setRemark_cn(museumModel.remark_cn) else view.setRemark_cn("Unprovided")

            if (museumModel.remark_en !== "") view.setRemark_en(museumModel.remark_en) else view.setRemark_en("Unprovided")
        }
    }
}