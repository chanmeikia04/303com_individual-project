package com.example.a303_project

import android.content.Context
import com.example.a303_project.ui.map.*
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class MapDetailPresenterTest {
    lateinit var mapDetailPresenter: MapDetailPresenter

    @RelaxedMockK
    lateinit var mapDetailModel: MapDetailModel

    @RelaxedMockK
    lateinit var view: MapDetailPresenter.View

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        mapDetailPresenter = MapDetailPresenter(view, mapDetailModel, context)
    }

    @After
    fun tearDown(){ clearAllMocks()}

    @Test
    fun testStart(){
        //Given
        val mockData_sid = "Start Point ID"
        val mockData_did = "Destination ID"

        //When
        mapDetailPresenter.start(mockData_sid, mockData_did)
        //Then
        verify {
            if (mockData_sid != null && mockData_did != null) {
                mapDetailModel.genFloorPlan(mockData_sid, mockData_did)
            } else {
                view.displayToastMessage("Error! Please go back prev page.")
            }
        }
    }

    @Test
    fun testDisplayToastMessage(){
        //Given
        val mockData = "testing message"
        //When
        mapDetailPresenter.displayToastMessage(mockData)
        //Then
        verify {  view.displayToastMessage(mockData) }
    }

    @Test
    fun testDrawFloorPlan(){
        //Given
        val mockData = MapDetailDBModel(0,0,0,0,"")
        //When
        mapDetailPresenter.drawFloorPlan(mockData)
        //Then
        verify {  view.drawFloorPlan(mockData) }
    }


}