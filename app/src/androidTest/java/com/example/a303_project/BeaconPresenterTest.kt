package com.example.a303_project

import android.content.Context
import com.example.a303_project.ui.Museum.MuseumListPresenter
import com.example.a303_project.ui.Museum.MuseumModel
import com.example.a303_project.ui.introduction.BeaconsInfoDBModel
import com.example.a303_project.ui.introduction.BeaconsModel
import com.example.a303_project.ui.introduction.IntroductionPresenter
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class BeaconPresenterTest {
     lateinit var introductionPresenter: IntroductionPresenter

    @RelaxedMockK
    lateinit var beaconsModel: BeaconsModel
    @RelaxedMockK
    lateinit var view: IntroductionPresenter.View
    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        introductionPresenter = IntroductionPresenter(view, context)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun testDisplayListToView() {
        // Given
        val mockData = arrayListOf(BeaconsInfoDBModel("01","","","","","",""))
        // When
        introductionPresenter.displayListToView(mockData)
        // Then
       verify {
           view.displayList(mockData)
       }
    }
}