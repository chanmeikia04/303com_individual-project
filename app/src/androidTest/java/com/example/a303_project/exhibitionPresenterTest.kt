package com.example.a303_project

import android.content.Context
import android.content.Intent
import com.example.a303_project.ui.exhibition.*
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class exhibitionPresenterTest {
    lateinit var exhibitionPresenter: ExhibitionPresenter

    @RelaxedMockK
    lateinit var exhibitionDBModel: ExhibitionDBModel

    @RelaxedMockK
    lateinit var view: ExhibitionPresenter.View

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp(){
        MockKAnnotations.init(this)

        exhibitionPresenter = ExhibitionPresenter(exhibitionDBModel, view, context)
    }

    @After
    fun tearDown(){ clearAllMocks() }

    @Test
    fun testStartToGetExhibitionList(){
        //Given

        //When
        exhibitionPresenter.startToGetExhibitionList()
        //Then
        verify { exhibitionDBModel.getInfoData() }
    }

    @Test
    fun testDisplayExhibitionListToView() {
        // Given
        val mockData = arrayListOf(ExhibitionModel("","","","","","","","","",false))
        // When
        exhibitionPresenter.displayExhibitionListToView(mockData)
        // Then
        verify { view.displayExhibitionList(mockData) }
    }

    @Test
    fun testDisplayToastMessageOnView(){
        //Given
        val mockData = "testing message"
        //When
        exhibitionPresenter.displayToastMessageOnView(mockData)
        //Then
        verify {  view.displayToastMessage(mockData) }
    }

    @Test
    fun testIntentToExhibitionDetail(){
        //Given
        val mockData = Intent(context, ExhibitionDetail::class.java)
        //When
        exhibitionPresenter.intentToExhibitionDetail(mockData)
        //Then
        verify { view.intentToDetail(mockData) }
    }
}