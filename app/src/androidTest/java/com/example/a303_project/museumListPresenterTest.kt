package com.example.a303_project

import android.content.Context
import android.content.Intent
import com.example.a303_project.ui.Museum.MuseumDBModel
import com.example.a303_project.ui.Museum.MuseumDetailActivity
import com.example.a303_project.ui.Museum.MuseumListPresenter
import com.example.a303_project.ui.Museum.MuseumModel
import com.example.a303_project.ui.exhibition.ExhibitionList
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class museumListPresenterTest {
    lateinit var museumListPresenter: MuseumListPresenter

    @RelaxedMockK
    lateinit var museumModel: MuseumModel

    @RelaxedMockK
    lateinit var museumDBModel: MuseumDBModel

    @RelaxedMockK
    lateinit var view: MuseumListPresenter.View

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        museumListPresenter = MuseumListPresenter(museumDBModel, view, context)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun testStartToGetList(){
        //Given

        //When
        museumListPresenter.startToGetList()

        //Then
        verify {
            museumDBModel.getInfoData()
        }
    }

    @Test
    fun testDisplayListToView() {
        // Given
        val mockData = arrayListOf(MuseumModel("01","","","","","","","","","","",""))
        // When
        museumListPresenter.displayListToView(mockData)
        // Then
        verify { view.displayList(mockData) }
    }

    @Test
    fun testDisplayToastMessageOnView(){
        //Given
        val mockData = "testing message"
        //When
        museumListPresenter.displayToastMessageOnView(mockData)
        //Then
        verify {  view.displayToastMessage(mockData) }
    }

    @Test
    fun testIntentToExhibitionList(){
        //Given
        val mockData = Intent(context, ExhibitionList::class.java)
        //When
        museumListPresenter.intentToExhibitionList(mockData)
        //Then
        verify { view.intentToExhibitionList(mockData) }
    }

    @Test
    fun testIntentToMuseumDetail(){
        //Given
        val mockData = Intent(context, MuseumDetailActivity::class.java)
        //When
        museumListPresenter.intentToMuseumDetail(mockData)
        //Then
        verify { view.intentToMuseumInfo(mockData) }
    }
}