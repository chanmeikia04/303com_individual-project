package com.example.a303_project

import android.content.Context
import android.content.Intent
import android.widget.Spinner
import com.example.a303_project.ui.Museum.MuseumDetailActivity
import com.example.a303_project.ui.introduction.BeaconsInfoDBModel
import com.example.a303_project.ui.introduction.BeaconsModel
import com.example.a303_project.ui.introduction.IntroductionPresenter
import com.example.a303_project.ui.map.MapModel
import com.example.a303_project.ui.map.MapPresenter
import com.example.a303_project.ui.map.SpinnerData
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class MapPresenterTest {
    lateinit var mapPresenter: MapPresenter

    @RelaxedMockK
    lateinit var mapModel:MapModel

    @RelaxedMockK
    lateinit var view: MapPresenter.View

    @RelaxedMockK
    lateinit var context: Context

    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        mapPresenter = MapPresenter(view, mapModel, context)
    }

    @After
    fun tearDown(){ clearAllMocks()}

    @Test
    fun testDisplayToastMessageOnView(){
        //Given
        val mockData = "testing message"
        //When
        mapPresenter.displayToastMessageOnView(mockData)
        //Then
        verify {  view.displayToastMessage(mockData) }
    }

    @Test
    fun testDisplayCurrentPosition(){
        //Given
        val mockData_id = "testing id"
        val mockData_msg = "testing message"
        //When
        mapPresenter.displayCurrentPosition(mockData_id, mockData_msg)
        //Then
        verify {  view.displayCurrentPosition(mockData_id, mockData_msg) }
    }

    @Test
    fun testDisplayDestinationList(){
        //Given
        val mockData = arrayListOf(SpinnerData("",""))
        //When
        mapPresenter.displayDestinationList(mockData)
        //Then
        verify {  view.displayDestinationList(mockData) }
    }

}