package com.example.a303_project.ui.exhibition;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.a303_project.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class ExhibitionRecyclerViewAdapter extends RecyclerView.Adapter<ExhibitionRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "RecyclerViewAdapter - Exhibition";
    private static ArrayList<ExhibitionModel> localDataSet;
    private static ExhibitionPresenter presenter;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvExhibitionName_cn, tvExhibitionName_en;
        private TextView tvExhibitionType_cn, tvExhibitionType_en;
        private ImageView ivPic;
        private LinearLayout llExhibitionList;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvExhibitionName_cn = view.findViewById(R.id.exhibition_name_cn);
            tvExhibitionName_en = view.findViewById(R.id.exhibition_name_en);

            tvExhibitionType_cn = view.findViewById(R.id.exhibition_type_cn);
            tvExhibitionType_en = view.findViewById(R.id.exhibition_type_en);

            ivPic = view.findViewById((R.id.exhibition_pic));

            llExhibitionList = view.findViewById(R.id.itemOfMuseumList);

            llExhibitionList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ExhibitionModel info = localDataSet.get(getAdapterPosition());

                    //Intent to exhibition detail information
                    Intent intent = new Intent(view.getContext(), ExhibitionDetail.class);
                    intent.putExtra("info", info);

                    presenter.intentToExhibitionDetail(intent);
                }
            });
        }

        public TextView getTvExhibitionType_cn() { return tvExhibitionType_cn; }
        public TextView getTvExhibitionType_en() { return tvExhibitionType_en; }
        public TextView getTvName_cn() { return tvExhibitionName_cn; }
        public TextView getTvName_en() { return tvExhibitionName_en; }
        public ImageView getIvPic() { return ivPic; }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public ExhibitionRecyclerViewAdapter(ArrayList<ExhibitionModel> dataSet, ExhibitionPresenter presenter) {
        localDataSet = dataSet;
        this.presenter = presenter;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.exhibition_list_row_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.getTvName_cn().setText(localDataSet.get(position).getName_cn());
        viewHolder.getTvName_en().setText(localDataSet.get(position).getName_en());
        if(localDataSet.get(position).getSpecialExhibition()) {
            viewHolder.getTvExhibitionType_cn().setText("專題展覽");
            viewHolder.getTvExhibitionType_en().setText("Special Exhibition");
        }else{
            viewHolder.getTvExhibitionType_cn().setText("常設展覽");
            viewHolder.getTvExhibitionType_en().setText("Permanent Exhibition");
        }

        if(localDataSet.get(position).getPic_url()!="") {
            Glide.with(viewHolder.itemView.getContext()).load(localDataSet.get(position).getPic_url()).into(viewHolder.getIvPic());
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }

}
