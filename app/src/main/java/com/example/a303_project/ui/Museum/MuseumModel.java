package com.example.a303_project.ui.Museum;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MuseumModel implements Serializable {
    private final String TAG = "HomeModel";
    private String id;
    private String name_en, name_cn;
    private String address_en, address_cn;
    private String openingHours;
    private String telephone;
    private String website, pic_url, floorPlan;
    private String remark_en, remark_cn;

    //constructor
    public MuseumModel() {
    }

    public MuseumModel(String id, String name_en, String name_cn, String address_en, String address_cn, String openingHours, String telephone, String website, String pic_url, String remark_en, String remark_cn, String floorPlan) {
        this.id = id;
        this.name_en = name_en;
        this.name_cn = name_cn;
        this.address_en = address_en;
        this.address_cn = address_cn;
        this.openingHours = openingHours;
        this.telephone = telephone;
        this.website = website;
        this.pic_url = pic_url;
        this.remark_en = remark_en;
        this.remark_cn = remark_cn;
        this.floorPlan = floorPlan;
    }

    //getter, setting
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_cn() {
        return name_cn;
    }

    public void setName_cn(String name_cn) {
        this.name_cn = name_cn;
    }

    public String getAddress_en() {
        return address_en;
    }

    public void setAddress_en(String address_en) {
        this.address_en = address_en;
    }

    public String getAddress_cn() {
        return address_cn;
    }

    public void setAddress_cn(String address_cn) {
        this.address_cn = address_cn;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getRemark_en() {
        return remark_en;
    }

    public void setRemark_en(String remark_en) {
        this.remark_en = remark_en;
    }

    public String getRemark_cn() {
        return remark_cn;
    }

    public void setRemark_cn(String remark_cn) {
        this.remark_cn = remark_cn;
    }

    public String getPic_url() { return pic_url; }

    public void setPic_url(String pic_url) { this.pic_url = pic_url; }

    public String getFloorPlan() { return floorPlan; }

    public void setFloorPlan(String floorPlan) { this.floorPlan = floorPlan; }
}
