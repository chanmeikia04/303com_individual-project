package com.example.a303_project.ui.exhibition;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a303_project.R;

import java.util.ArrayList;

public class ExhibitionList extends AppCompatActivity implements ExhibitionPresenter.View{
    private final String TAG = "ExhibitionList";
    private ExhibitionPresenter exhibitionPresenter;

    //Recycler View
    private RecyclerView recycler_view;
    private ExhibitionRecyclerViewAdapter exhibitionRecyclerViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibition_list);
        Log.d(TAG, "Exhibition");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getIntent().getStringExtra("name_cn") + " " + getIntent().getStringExtra("name_en"));

        exhibitionPresenter = new ExhibitionPresenter(this, getIntent().getStringExtra("museumId"), this);
        exhibitionPresenter.startToGetExhibitionList();

        recycler_view = findViewById(R.id.recycler_view_exhibition);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

//    @Override
//    public void displayExhibitionList(ArrayList<ExhibitionModel> mData, ExhibitionPresenter presenter) {
//        exhibitionRecyclerViewAdapter = new ExhibitionRecyclerViewAdapter(mData, presenter);
//        recycler_view.setAdapter(exhibitionRecyclerViewAdapter);
//    }

    @Override
    public void displayExhibitionList(ArrayList<ExhibitionModel> mData) {
        exhibitionRecyclerViewAdapter = new ExhibitionRecyclerViewAdapter(mData, exhibitionPresenter);
        recycler_view.setAdapter(exhibitionRecyclerViewAdapter);
    }

    @Override
    public void intentToDetail(Intent intent) { startActivity(intent); }

    @Override
    public void displayToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}