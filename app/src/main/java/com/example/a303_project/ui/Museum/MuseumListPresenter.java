package com.example.a303_project.ui.Museum;

import android.content.Context;
import android.content.Intent;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

public class MuseumListPresenter {
    private final String TAG = "HomePresenter";

    //Model, View and Context
    MuseumDBModel dbModel;
    MuseumListPresenter.View view;
    Context context;

    //Constructor
    public MuseumListPresenter(@NotNull MuseumListPresenter.View view, Context context) {
        dbModel = new MuseumDBModel(this);
        this.view = view;
        this.context = context;
    }

    @TestOnly
    public MuseumListPresenter(MuseumDBModel dbModel, MuseumListPresenter.View view, Context context) {
        this.dbModel = dbModel;
        this.view = view;
        this.context = context;
    }

    //Call museum list
    public void startToGetList(){
        dbModel.getInfoData();
    }

    public void displayToastMessageOnView(String msg){ view.displayToastMessage(msg); }

    public void displayListToView(ArrayList<MuseumModel> mData){
        view.displayList(mData);
    }

    public void intentToExhibitionList(Intent intent) {
        view.intentToExhibitionList(intent);
    }

    public void intentToMuseumDetail(Intent intent) { view.intentToMuseumInfo(intent); }

    public interface View {
        void displayList(ArrayList<MuseumModel> mData);
        void intentToExhibitionList(Intent intent);
        void intentToMuseumInfo(Intent intent);
        void displayToastMessage(String msg);
    }
}
