package com.example.a303_project.ui.Museum;

import android.util.Log;

public class MuseumDetailPresenter {
    private final String TAG = "MuseumDetailPresenter";
    MuseumDetailPresenter.View view;
    MuseumModel museumModel;

    public MuseumDetailPresenter(View view, MuseumModel museumModel) {
        this.view = view;
        this.museumModel = museumModel;
    }

    public void displayData(){
        //Name
        view.setName_cn(museumModel.getName_cn());
        view.setName_en(museumModel.getName_en());

        //Opening Hours
        if(museumModel.getOpeningHours() != "")
            view.setHour(museumModel.getOpeningHours());
        else
            view.setHour("Unprovided");

        //Tel
        view.setTel(museumModel.getTelephone());

        //Website
        view.setWebsite(museumModel.getWebsite());
        view.setWebsiteLink(museumModel.getWebsite());

        //Address
        view.setAddress_cn(museumModel.getAddress_cn());
        view.setAddress_en(museumModel.getAddress_en());

        //Remark
        if(museumModel.getRemark_cn() != "")
            view.setRemark_cn(museumModel.getRemark_cn());
        else
            view.setRemark_cn("Unprovided");

        if (museumModel.getRemark_en()!= "")
            view.setRemark_en(museumModel.getRemark_en());
        else
            view.setRemark_en("Unprovided");
    }

    public interface View {
        void setName_cn(String name_cn);
        void setName_en(String name_en);
        void setHour(String openingHours);
        void setTel(String telephone);
        void setWebsite(String website);
        void setAddress_cn(String address_cn);
        void setAddress_en(String address_en);
        void setRemark_cn(String remark_cn);
        void setRemark_en(String remark_en);
        void setWebsiteLink(String website);
    }
}
