package com.example.a303_project.ui.map;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.a303_project.R;
import com.example.a303_project.ui.introduction.BeaconsInfoDBModel;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;


public class MapModel implements BeaconConsumer {
    protected static final String TAG = "MapModel";
    private  MapPresenter presenter;

    private BeaconManager beaconManager;
    private int beaconNum = 0;
    private String currPosition_ma = null;
    private String currPosition_mi = null;
    private String currUUID = null;
    private double currDistance = 9999;

    public MapModel(MapPresenter presenter) {
        this.presenter = presenter;
    }

    public void getBeaconList(){
        beaconManager = BeaconManager.getInstanceForApplication(presenter.context);
        beaconManager.setForegroundScanPeriod(1000L);
        beaconManager.setForegroundBetweenScanPeriod(100000L);
        beaconManager.setEnableScheduledScanJobs(true);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.bind(this);
        Log.i(TAG, "Start to Scan Beacons");
        presenter.displayToastMessageOnView(presenter.context.getString(R.string.Scanning));
    }


    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {

            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                beaconNum = beacons.size();
                if (beacons.size() > 0) {
                    for (Beacon beacon : beacons) {
                        if(currDistance > beacon.getDistance()){
                            currDistance = beacon.getDistance();
                            currUUID = beacon.getId1().toString();
                            currPosition_ma = beacon.getId2().toString();
                            currPosition_mi = beacon.getId3().toString();
                        }
                        beaconNum--;
                    }
                    if(beaconNum == 0) {
                        getBeaconInfoData();
                        getDestinInfo();
                    }
                }
            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {
            Log.i(TAG, e.toString());
        }
    }

    //    172.20.10.2 192.168.1.154
    public void getBeaconInfoData() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://172.20.10.2:3000/mapBeaconApi/" + currUUID + "/" + currPosition_mi + "/" + currPosition_ma,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
//                        Log.d(TAG, "Result:" + response.toString());
                        try {
                            if (response.length() < 0) {
                                presenter.displayToastMessageOnView(presenter.context.getString(R.string.ErrorMsg_NoDetectBeacon));
                            } else {
                                JSONObject o = response.getJSONObject(0);
                                presenter.displayCurrentPosition(o.getString("areaID"), o.getString("area_cn") + " " + o.getString("area_en"));
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "Error Msg：JSON" + e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Msg：JSON - " + error.toString());
                presenter.displayToastMessageOnView(presenter.context.getString(R.string.ErrorMsg_NoConnectToServer));
            }
        });
        Volley.newRequestQueue(presenter.context).add(jsonArrayRequest);
    }

    public void getDestinInfo() {
        //    172.20.10.2 192.168.1.154
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://172.20.10.2:3000/museumAreaApi/",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
//                        Log.d(TAG, "Result:" + response.toString());
                        try {
                            if (response.length() > 0) {
                                ArrayList<SpinnerData> destinationList = new ArrayList<SpinnerData>();

                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject q = response.getJSONObject(i);

                                    destinationList.add(new SpinnerData(q.getString("id") ,q.getString("area_cn") + " " + q.getString("area_en")));
                                }

                                presenter.displayDestinationList(destinationList);
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "Error Msg：JSON" + e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Msg：JSON - " + error.toString());
                presenter.displayToastMessageOnView(presenter.context.getString(R.string.ErrorMsg_NoConnectToServer));
            }
        });
        Volley.newRequestQueue(presenter.context).add(jsonArrayRequest);
    }

    @Override
    public Context getApplicationContext() { return null; }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {}

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) { return false; }
}
