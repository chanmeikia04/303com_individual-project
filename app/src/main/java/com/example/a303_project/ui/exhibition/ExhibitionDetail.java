package com.example.a303_project.ui.exhibition;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.a303_project.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class ExhibitionDetail extends AppCompatActivity implements ExhibitionDetailPresenter.View{
    private final String TAG = "ExhibitionDetail";
    private ExhibitionDetailPresenter presenter;
    private TextView tvName_cn, tvName_en, tvDate, tvIntro_cn, tvIntro_en, tvType;
    private ImageView ivExhibitionDetail_pic;
    private LinearLayout llExhibitionDate;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibition_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab = findViewById(R.id.fab);
        tvName_cn = findViewById(R.id.tvExhibitionDetail_name_cn);
        tvName_en = findViewById(R.id.tvExhibitionDetail_name_en);
        tvDate = findViewById(R.id.tvExhibitionDetail_Date);
        tvIntro_cn = findViewById(R.id.tvExhibitionDetail_Intro_cn);
        tvIntro_en = findViewById(R.id.tvExhibitionDetail_Intro_en);
        tvType = findViewById(R.id.tvExhibitionDetail_type);
        ivExhibitionDetail_pic = findViewById(R.id.ivExhibitionDetail_pic);
        llExhibitionDate = findViewById(R.id.llExhibitionDate);

        ExhibitionModel exhibitionModel = (ExhibitionModel) getIntent().getSerializableExtra("info");
        setTitle(exhibitionModel.getName_cn() + " " + exhibitionModel.getName_en());

        presenter = new ExhibitionDetailPresenter(this, exhibitionModel);
        presenter.displayData();

        Log.d(TAG, exhibitionModel.getExhibitionID());
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

    @Override
    public void setName_cn(String name_cn) { tvName_cn.setText(name_cn); }

    @Override
    public void setName_en(String name_en) { tvName_en.setText(name_en); }

    @Override
    public void setDate(String s) { tvDate.setText(s); }

    @Override
    public void setType(String s) { tvType.setText(s);}

    @Override
    public void setIntro_cn(String intro_cn) { tvIntro_cn.setText(intro_cn);}

    @Override
    public void setIntro_en(String intro_en) { tvIntro_en.setText(intro_en);}

    @Override
    public void setPic(String pic_url) { Glide.with(this).load(pic_url).into(ivExhibitionDetail_pic);}

    @Override
    public void setWebsite(String website_url) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website_url));
                startActivity(browserIntent);
            }
        });
    }

    @Override
    public void hiddenDate() {
        llExhibitionDate.setVisibility(View.GONE);
    }
}