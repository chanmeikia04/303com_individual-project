package com.example.a303_project.ui.Museum;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a303_project.R;

import java.util.ArrayList;


public class MuseumListFragment extends Fragment implements MuseumListPresenter.View{
    private final String TAG = "HomePage";

    //Presenter
    private MuseumListPresenter homePresenter;

    //Recycler View
    private RecyclerView recycler_view;
    private RecyclerViewAdapter adapter;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        //Setup presenter
        homePresenter = new MuseumListPresenter(this, getContext());

        //Start to call the museum list
        homePresenter.startToGetList();

        //Setup Recycler View
        recycler_view = root.findViewById(R.id.recycler_view_home);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_view.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        return root;
    }


    //Display museum list with recycler view adapter
    @Override
    public void displayList(ArrayList<MuseumModel> mData) {
        adapter = new RecyclerViewAdapter(mData, homePresenter);
        recycler_view.setAdapter(adapter);
    }

    //Display Toast Message
    @Override
    public void displayToastMessage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void intentToExhibitionList(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void intentToMuseumInfo(Intent intent) {
        startActivity(intent);
    }
}