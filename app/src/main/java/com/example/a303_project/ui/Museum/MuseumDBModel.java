package com.example.a303_project.ui.Museum;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.a303_project.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MuseumDBModel {
    private final String TAG = "DBModel";
    private MuseumListPresenter presenter;
    ArrayList <MuseumModel> mData = new ArrayList<MuseumModel>();

    //constructor
    public MuseumDBModel(MuseumListPresenter presenter) {
        this.presenter = presenter;
        mData = new ArrayList<>();
    }

    //Get Json from server
    public void getInfoData() {
//        192.168.1.154
//        172.20.10.2
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://192.168.1.154:3000/api/museums",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
//                        Log.d(TAG, "Result: " + response.toString());
                        try {
                            getMuseumInfo(response);
                        } catch (JSONException e) {
                            Log.e(TAG, "Error：JSON - " + e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error：JSON - " + error.toString());
                presenter.displayToastMessageOnView(presenter.context.getString(R.string.ErrorMsg_NoConnectToServer));
            }
        });
        Volley.newRequestQueue(presenter.context).add(jsonArrayRequest);
    }

    //Put the result to MuseumModel -> ArrayList and display
    private void getMuseumInfo(JSONArray rs) throws JSONException {
        if (rs.length() < 0) {

        } else {
            for (int i = 0; i < rs.length(); i++) {
                JSONObject o = rs.getJSONObject(i);
                MuseumModel homeModel = new MuseumModel();
                homeModel.setId(o.getString("id"));

                homeModel.setName_cn(o.getString("name_cn"));
                homeModel.setName_en(o.getString("name_en"));

                homeModel.setOpeningHours(o.getString("openingHours"));
                homeModel.setTelephone(o.getString("telephone"));
                homeModel.setWebsite(o.getString("website"));
                homeModel.setPic_url(o.getString("pic_url"));

                homeModel.setAddress_cn(o.getString("address_cn"));
                homeModel.setAddress_en(o.getString("address_en"));

                homeModel.setRemark_cn(o.getString("remark_cn"));
                homeModel.setRemark_en(o.getString("remark_en"));

                homeModel.setFloorPlan(o.getString("floorPlan"));

                mData.add(homeModel);
            }
            presenter.displayListToView(mData);
        }
    }
}
