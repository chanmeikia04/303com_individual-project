package com.example.a303_project.ui.Museum;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.a303_project.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MuseumDetailActivity extends AppCompatActivity implements MuseumDetailPresenter.View{
    private final String TAG = "MuseumDetailActivity";
    private MuseumDetailPresenter museumDetailPresenter;
    private TextView tvName_cn, tvName_en, tvHour, tvTel, tvWebsite, tvAddress_cn, tvAddress_en, tvRemark_cn, tvRemark_en;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab = findViewById(R.id.museum_fab);
        tvAddress_en = findViewById(R.id.museumDetail_address_en);
        tvAddress_cn = findViewById(R.id.museumDetail_address_cn);
        tvHour = findViewById(R.id.museumDetail_openingHours);
        tvTel = findViewById(R.id.museumDetail_tel);
        tvWebsite = findViewById(R.id.museumDetail_website);
        tvName_cn = findViewById(R.id.museumDetail_name_cn);
        tvName_en = findViewById(R.id.museumDetail_name_en);
        tvRemark_cn = findViewById(R.id.museumDetail_remark_cn);
        tvRemark_en = findViewById(R.id.museumDetail_remark_en);

        MuseumModel museumModel = (MuseumModel)getIntent().getSerializableExtra("museumInfo");
        setTitle(museumModel.getName_cn()+" " + museumModel.getName_en());

        museumDetailPresenter = new MuseumDetailPresenter(this, museumModel);
        museumDetailPresenter.displayData();
    }

    @Override
    public void setName_cn(String name_cn) { tvName_cn.setText(name_cn); }

    @Override
    public void setName_en(String name_en) { tvName_en.setText(name_en);}

    @Override
    public void setHour(String openingHours) { Log.d(TAG, "opening" + openingHours); tvHour.setText(openingHours);}

    @Override
    public void setTel(String telephone) { tvTel.setText(telephone);}

    @Override
    public void setWebsite(String website) { tvWebsite.setText(website);}

    @Override
    public void setAddress_cn(String address_cn) { tvAddress_cn.setText(address_cn);}

    @Override
    public void setAddress_en(String address_en) { tvAddress_en.setText(address_en);}

    @Override
    public void setRemark_cn(String remark_cn) { tvRemark_cn.setText(remark_cn);}

    @Override
    public void setRemark_en(String remark_en) { tvRemark_en.setText(remark_en);}

    @Override
    public void setWebsiteLink(String website) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
                startActivity(browserIntent);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }
}