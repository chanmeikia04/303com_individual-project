package com.example.a303_project.ui.map;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.a303_project.R;

import java.util.ArrayList;

public class MapFragment extends Fragment implements MapPresenter.View {
    protected static final String TAG = "MapFragment";
    private EditText edCurrPosition;
    private Spinner spDestination;
    private Button btnFind;
    private String currenyPoID;

    //Presenter
    private MapPresenter mapPresenter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_map, container, false);


        //Setup presenter
        mapPresenter = new MapPresenter(this, getContext());
        mapPresenter.start();

        edCurrPosition = root.findViewById(R.id.etCurrPosition);
        spDestination = root.findViewById(R.id.spinnerDestination);
        btnFind = root.findViewById(R.id.btnFind);
        btnFind.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapPresenter.checkLoc(spDestination, currenyPoID);
            }
        });

        return root;
    }

    @Override
    public void displayToastMessage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayCurrentPosition(String id, String msg) {
        edCurrPosition.setText(msg);
        currenyPoID = id;
    }

    @Override
    public void displayDestinationList(ArrayList<SpinnerData> destinationList) {
        Log.d(TAG, "Sett");
        ArrayAdapter<SpinnerData> adapter = new ArrayAdapter<SpinnerData>(getContext(), android.R.layout.simple_spinner_dropdown_item, destinationList);
        spDestination.setAdapter(adapter);
    }

    @Override
    public void intentDetail(Intent intent) {
        startActivity(intent);
    }
}