package com.example.a303_project.ui.introduction;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.a303_project.R;

import java.util.ArrayList;

public class IntroductionRecyclerViewAdapter extends RecyclerView.Adapter<IntroductionRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "RecyclerViewAdapter";
    private static ArrayList<BeaconsInfoDBModel> localDataSet;
    private static IntroductionPresenter presenter;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvname_cn, tvname_en;
        private Button btnLink;
        private ImageView ivPic;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvname_cn = view.findViewById(R.id.introduction_name_cn);
            tvname_en = view.findViewById(R.id.introduction_name_en);
            ivPic = view.findViewById(R.id.ivBeaconExhibitionPic);
            btnLink = view.findViewById(R.id.btnIntroductionLink);

            btnLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "OnClick ");
                    //Intent to exhibition information
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(localDataSet.get(getAdapterPosition()).getWebsite_url()));
                    presenter.intentToOther(intent);
                }
            });

        }


        public TextView getTvName_cn() { return tvname_cn; }
        public TextView getTvName_en() { return tvname_en; }
        public ImageView getIvPic() { return ivPic; }
    }

    public IntroductionRecyclerViewAdapter(ArrayList<BeaconsInfoDBModel> dataSet, IntroductionPresenter presenter) {
        localDataSet = dataSet;
        this.presenter = presenter;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.introduction_row_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.getTvName_cn().setText(localDataSet.get(position).getName_cn());
        viewHolder.getTvName_en().setText(localDataSet.get(position).getName_en());
        if(localDataSet.get(position).getPic_url()!="") {
            Glide.with(viewHolder.itemView.getContext()).load(localDataSet.get(position).getPic_url()).into(viewHolder.getIvPic());
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }

}
