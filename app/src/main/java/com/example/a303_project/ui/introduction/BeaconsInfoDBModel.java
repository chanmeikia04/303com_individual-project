package com.example.a303_project.ui.introduction;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.a303_project.ui.Museum.MuseumListPresenter;
import com.example.a303_project.ui.Museum.MuseumModel;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BeaconsInfoDBModel {
    private final String TAG = "BeaconsInfoDBModel";
    private String pic_url, website_url, major, uuid, minor, name_cn, name_en;



    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getName_cn() {
        return name_cn;
    }

    public void setName_cn(String name_cn) {
        this.name_cn = name_cn;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public BeaconsInfoDBModel() {
    }

    public BeaconsInfoDBModel(String pic_url, String website_url, String major, String uuid, String minor, String name_cn, String name_en) {
        this.pic_url = pic_url;
        this.website_url = website_url;
        this.major = major;
        this.uuid = uuid;
        this.minor = minor;
        this.name_cn = name_cn;
        this.name_en = name_en;
    }
}
