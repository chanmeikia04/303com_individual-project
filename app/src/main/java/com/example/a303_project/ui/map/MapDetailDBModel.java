package com.example.a303_project.ui.map;

public class MapDetailDBModel {
    protected static final String TAG = "MapDetailDBModel";

    private int sX, sY;
    private int dX, dY;
    private String floorPlan;

    public MapDetailDBModel() {
    }

    public MapDetailDBModel(int sX, int sY, int dX, int dY, String floorPlan) {
        this.sX = sX;
        this.sY = sY;
        this.dX = dX;
        this.dY = dY;
        this.floorPlan = floorPlan;
    }

    public int getsX() {
        return sX;
    }

    public void setsX(int sX) {
        this.sX = sX;
    }

    public int getsY() {
        return sY;
    }

    public void setsY(int sY) {
        this.sY = sY;
    }

    public int getdX() {
        return dX;
    }

    public void setdX(int dX) {
        this.dX = dX;
    }

    public int getdY() {
        return dY;
    }

    public void setdY(int dY) {
        this.dY = dY;
    }

    public String getFloorPlan() {
        return floorPlan;
    }

    public void setFloorPlan(String floorPlan) {
        this.floorPlan = floorPlan;
    }
}
