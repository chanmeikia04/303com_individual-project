package com.example.a303_project.ui.exhibition;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.a303_project.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExhibitionDBModel {
    private final String TAG = "DBModelExhibition";
    private ExhibitionPresenter presenter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String museumId;
    private ArrayList<ExhibitionModel> mData;


    //constructor
    public ExhibitionDBModel(ExhibitionPresenter presenter, String museumId) {
        this.presenter = presenter;
        this.museumId = museumId;
        mData = new ArrayList<ExhibitionModel>();
    }

//    192.168.1.154
//    172.20.10.2

    public void getInfoData() {
//        Log.d(TAG, "museumId: " + "http://172.20.10.2:3000/exhibitionApi/"+museumId);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://192.168.1.154:3000/exhibitionApi/"+museumId,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "Result: " + response.toString());
                        try {
                            getExhibitionInfo(response);
                        } catch (JSONException e) {
                            Log.e(TAG, "Error：JSON - " + e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error：JSON - " + error.toString());
                presenter.displayToastMessageOnView(presenter.context.getString(R.string.ErrorMsg_NoConnectToServer));

            }
        });
        Volley.newRequestQueue(presenter.context).add(jsonArrayRequest);
    }

    private void getExhibitionInfo(JSONArray rs) throws JSONException {

        if (rs.length() < 0) {
//            carParkPresenter.sendErrorMessage("CountOfRecord");
        } else {
            for (int i = 0; i < rs.length(); i++) {
                JSONObject o = rs.getJSONObject(i);

//                String str = "ID: " + o.getString("park_Id") + "\n";
                ExhibitionModel exhibitionModel = new ExhibitionModel();
                exhibitionModel.setExhibitionID(o.getString("id"));
                exhibitionModel.setMuseumId(o.getString("museumId"));

                exhibitionModel.setName_cn(o.getString("name_cn"));
                exhibitionModel.setName_en(o.getString("name_en"));

                exhibitionModel.setIntro_cn(o.getString("intro_cn"));
                exhibitionModel.setIntro_en(o.getString("intro_en"));

                exhibitionModel.setDate(o.getString("date"));

                exhibitionModel.setSpecialExhibition(o.getBoolean("isSpecialExhibition"));
                exhibitionModel.setPic_url(o.getString("pic_url"));
                exhibitionModel.setWebsite_url(o.getString("website_url"));
                mData.add(exhibitionModel);

            }
            presenter.displayExhibitionListToView(mData);
        }
    }

    public ArrayList<ExhibitionModel> getmData() {
        return mData;
    }
}
