package com.example.a303_project.ui.introduction;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.a303_project.R;
import com.example.a303_project.ui.Museum.MuseumModel;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class BeaconsModel implements BeaconConsumer {
    protected static final String TAG = "BeaconsModel";

    private IntroductionPresenter introductionPresenter;

    private BeaconManager beaconManager;
    private ArrayList <BeaconsInfoDBModel> mData = new ArrayList<BeaconsInfoDBModel>();
    private int beaconNum = 0;


    public BeaconsModel(IntroductionPresenter presenter){
        this.introductionPresenter = presenter;
        mData = new ArrayList<>();
    }

    public ArrayList<BeaconsInfoDBModel> getmData() {
        return mData;
    }
    public void setmData(ArrayList<BeaconsInfoDBModel> mData) {
        this.mData = mData;
    }

    public void getBeaconList(){
        beaconManager = BeaconManager.getInstanceForApplication(introductionPresenter.context);
        beaconManager.setForegroundScanPeriod(1000L);
        beaconManager.setForegroundBetweenScanPeriod(100000L);
        beaconManager.setEnableScheduledScanJobs(true);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.bind(this);
        Log.i(TAG, "Start to Scan Beacons");
        introductionPresenter.displayToastMessageOnView(introductionPresenter.context.getString(R.string.Scanning));
    }

    public void onDestroy() {
        beaconManager.unbind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {

            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                mData.clear();
                beaconNum = beacons.size();
                if (beacons.size() > 0) {
                    for (Beacon beacon : beacons) {
                        getBeaconInfoData(beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString());
                    }
                }
            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {
            Log.i(TAG, e.toString());
        }
    }

    @Override
    public Context getApplicationContext() {
        return null;
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {}

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) { return false; }

//    172.20.10.2 192.168.1.154
    public void getBeaconInfoData(String uuid, String major, String minor) {
//        Log.i(TAG,"http://192.168.1.154:3000/additionalInfoApi/" + uuid + "/" + minor + "/" + major);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://172.20.10.2:3000/additionalInfoApi/" + uuid + "/" + minor + "/" + major,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "Result:" + response.toString());
                        try {
                            getInfo(response);
                        } catch (JSONException e) {
                            Log.e(TAG, "Error Msg：JSON");
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Msg：JSON - " + error.toString());
                introductionPresenter.displayToastMessageOnView(introductionPresenter.context.getString(R.string.ErrorMsg_NoConnectToServer));
            }
        });
        Volley.newRequestQueue(introductionPresenter.context).add(jsonArrayRequest);
    }

    private void getInfo(JSONArray rs) throws JSONException {
        if (rs.length() < 0) {
            introductionPresenter.displayToastMessageOnView(introductionPresenter.context.getString(R.string.ErrorMsg_NoDetectBeacon));
        } else {
            for (int i = 0; i < rs.length(); i++) {
                JSONObject o = rs.getJSONObject(i);

                BeaconsInfoDBModel beaconsInfoDBModel = new BeaconsInfoDBModel();
                beaconsInfoDBModel.setUuid(o.getString("uuid"));
                beaconsInfoDBModel.setMajor(o.getString("major"));
                beaconsInfoDBModel.setMinor(o.getString("minor"));

                beaconsInfoDBModel.setName_cn(o.getString("name_cn"));
                beaconsInfoDBModel.setName_en(o.getString("name_en"));

                beaconsInfoDBModel.setPic_url(o.getString("pic_url"));
                beaconsInfoDBModel.setWebsite_url(o.getString("website_url"));


                mData.add(beaconsInfoDBModel);
                beaconNum--;
            }
            if(beaconNum==0) {
                introductionPresenter.displayListToView(mData);
                onDestroy();
            }
        }
    }
}
