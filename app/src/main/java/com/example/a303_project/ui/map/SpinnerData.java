package com.example.a303_project.ui.map;

public class SpinnerData {
    private String value;
    private String text;

    public SpinnerData() {
        value = "";
        text = "";
    }

    public SpinnerData(String value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
