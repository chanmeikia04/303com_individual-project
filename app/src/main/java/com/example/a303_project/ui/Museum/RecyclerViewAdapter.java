package com.example.a303_project.ui.Museum;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.a303_project.R;
import com.example.a303_project.ui.exhibition.ExhibitionList;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "RecyclerViewAdapter";
    private static ArrayList<MuseumModel> localDataSet;
    private static MuseumListPresenter presenter;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvname_cn, tvname_en;
        private Button btnMeseum, btnExhibition;
        private ImageView ivPic;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvname_cn = view.findViewById(R.id.home_name_cn);
            tvname_en = view.findViewById(R.id.home_name_en);
            btnExhibition = view.findViewById(R.id.btnExhibition);
            btnMeseum = view.findViewById(R.id.btnMuseumDetail);
            ivPic = view.findViewById(R.id.ivMuseumPic);

            btnExhibition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id = localDataSet.get(getAdapterPosition()).getId();
                    Log.d(TAG, "OnClick " + id);
                    //Intent to exhibition information
                    Intent intent = new Intent(view.getContext(), ExhibitionList.class);
                    intent.putExtra("museumId", id);
                    intent.putExtra("name_cn", localDataSet.get(getAdapterPosition()).getName_cn());
                    intent.putExtra("name_en", localDataSet.get(getAdapterPosition()).getName_en());
                    presenter.intentToExhibitionList(intent);
                }
            });

            btnMeseum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id = localDataSet.get(getAdapterPosition()).getId();
                    Log.d(TAG, "OnClick " + id);
                    //Intent to exhibition information
                    Intent intent = new Intent(view.getContext(), MuseumDetailActivity.class);
                    intent.putExtra("museumInfo", localDataSet.get(getAdapterPosition()));
                    presenter.intentToMuseumDetail(intent);
                }
            });
        }


        public TextView getTvName_cn() { return tvname_cn; }
        public TextView getTvName_en() { return tvname_en; }
        public ImageView getIvPic() { return ivPic; }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public RecyclerViewAdapter(ArrayList<MuseumModel> dataSet, MuseumListPresenter presenter) {
        localDataSet = dataSet;
        this.presenter = presenter;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_row_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.getTvName_cn().setText(localDataSet.get(position).getName_cn());
        viewHolder.getTvName_en().setText(localDataSet.get(position).getName_en());
        if(localDataSet.get(position).getPic_url()!="") {
            Glide.with(viewHolder.itemView.getContext()).load(localDataSet.get(position).getPic_url()).into(viewHolder.getIvPic());
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }

}
