package com.example.a303_project.ui.exhibition;

import android.content.Context;
import android.content.Intent;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

public class ExhibitionPresenter {
    private final String TAG = "ExhibitionPresenter";
    ExhibitionDBModel exhibitionDbModel;
    ExhibitionPresenter.View view;
    Context context;


    public ExhibitionPresenter(@NotNull ExhibitionPresenter.View view, String museumId, Context context) {
        this.view = view;
        exhibitionDbModel = new ExhibitionDBModel(this, museumId);
        this.context = context;
    }

    @TestOnly
    public ExhibitionPresenter(ExhibitionDBModel exhibitionDbModel, View view, Context context) {
        this.exhibitionDbModel = exhibitionDbModel;
        this.view = view;
        this.context = context;
    }

    public void startToGetExhibitionList() {
//        Log.d(TAG, "startToGetExhibitionList");
        exhibitionDbModel.getInfoData();
    }

    public void displayExhibitionListToView(ArrayList<ExhibitionModel> mData){
        view.displayExhibitionList(mData);
    }

    public void intentToExhibitionDetail(Intent intent) {
        view.intentToDetail(intent);
    }

    public void displayToastMessageOnView(String msg){ view.displayToastMessage(msg); }

    public interface View {
//        void displayExhibitionList(ArrayList<ExhibitionModel> mData, ExhibitionPresenter presenter);
        void displayExhibitionList(ArrayList<ExhibitionModel> mData);
        void intentToDetail(Intent intent);
        void displayToastMessage(String msg);
    }
}
