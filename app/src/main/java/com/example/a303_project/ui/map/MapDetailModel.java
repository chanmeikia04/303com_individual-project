package com.example.a303_project.ui.map;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.a303_project.R;
import com.example.a303_project.ui.Museum.MuseumModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapDetailModel {
    protected static final String TAG = "MapDetailModel";
    private MapDetailPresenter presenter;
    MapDetailDBModel dbModel;

    public MapDetailModel(MapDetailPresenter presenter) {
        this.presenter = presenter;
        dbModel = new MapDetailDBModel();
    }


    public void genFloorPlan(String startPointID, String destinationID) {
//        192.168.1.154
//        172.20.10.2
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                "http://172.20.10.2:3000/museumAreaApi/" + startPointID + "/" + destinationID,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Log.d(TAG, "Result: " + response.toString());
                        try {
                            getInfo(response);
                        } catch (JSONException e) {
                            Log.e(TAG, "Error：JSON - " + e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error：JSON - " + error.toString());
                presenter.displayToastMessage(presenter.context.getString(R.string.ErrorMsg_NoConnectToServer));
            }
        });
        Volley.newRequestQueue(presenter.context).add(jsonRequest);

    }

    private void getInfo(JSONObject rs) throws JSONException {
        if (rs.length() > 0) {
//            Log.i(TAG, rs.getString("dX"));
            dbModel.setdX(Integer.parseInt(rs.getString("dX")));
            dbModel.setdY(Integer.parseInt(rs.getString("dY")));
            dbModel.setsX(Integer.parseInt(rs.getString("sX")));
            dbModel.setsY(Integer.parseInt(rs.getString("sY")));
            dbModel.setFloorPlan(rs.getString("floorPlan"));
            presenter.drawFloorPlan(dbModel);
        }
    }
}
