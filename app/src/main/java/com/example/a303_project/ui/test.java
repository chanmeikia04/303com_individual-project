package com.example.a303_project.ui;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class test {
    String TAG = "test";
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public void add(){
        // Create a new user with a first and last name
        Map<String, Object> exhibition = new HashMap<>();
        exhibition.put("Date", "");
        exhibition.put("Intro_cn", "展覽室展出由不同學校或社區團體籌辦的專題展覽，介紹香港、特別是新界地區的歷史文化。");
        exhibition.put("Intro_en", "The Community Heritage Gallery presents thematic exhibitions arranged by schools and community organisations on the local history and culture of Hong Kong, especially in the New Territories.");
        exhibition.put("exhibitionName_cn", "社區文物展覽室");
        exhibition.put("exhibitionName_en", "The Community Heritage Gallery");
        exhibition.put("isSpecialExhibition", false);
        exhibition.put("museumId", "94SZVHWlZEnyXsmt495u");
        exhibition.put("pic_url", "https://firebasestorage.googleapis.com/v0/b/com-3592a.appspot.com/o/Exhibitions%2FTCHG.png?alt=media&token=5b401266-4f45-4319-aa4d-d6f9a65ab773");
        exhibition.put("website_url", "https://www.amo.gov.hk/en/pingshan_01_03.php");

//        Map<String, Object> exhibition = new HashMap<>();
//        exhibition.put("Date", "");
//        exhibition.put("Intro_cn", "");
//        exhibition.put("Intro_en", "");
//        exhibition.put("exhibitionName_cn", "");
//        exhibition.put("exhibitionName_en", "");
//        exhibition.put("isSpecialExhibition", true);
//        exhibition.put("museumId", "");
//        exhibition.put("pic_url", "");
//        exhibition.put("website_url", "");

// Add a new document with a generated ID
        db.collection("exhibitions")
                .add(exhibition)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }
}
