package com.example.a303_project.ui.introduction;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;

import android.util.Log;

import androidx.core.content.ContextCompat;

import com.example.a303_project.R;

import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

public class IntroductionPresenter extends Activity{

    private final String TAG = "IntroductionPresenter";
    private static final int REQUEST_CODE_ACCESS_COARSE_LOCATION = 1;

    IntroductionPresenter.View view;
    BeaconsModel model;
    Context context;

    public IntroductionPresenter(View view, Context context) {
        this.view = view;
        this.context = context;
        model = new BeaconsModel(this);
    }

    @TestOnly
    public IntroductionPresenter(View view, BeaconsModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
    }

    public void displayListToView(ArrayList <BeaconsInfoDBModel> mData) {
        view.displayList(mData);
    }

    public void intentToOther(Intent intent) {
        view.intentToOtherWebsite(intent);
    }

    public void start() {
        //BLE
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Location - GPS
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        //Check if GPS is authorized
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //Check if bluetooth is supported
            if (bluetoothAdapter == null) {
                view.displayToastMessage(context.getString(R.string.ErrorMsg_NotSupBluetooth));
            } else {
                //Check if GPS and Bluetooth is Turn on
                if ((gps || network) && bluetoothAdapter.isEnabled()) {
                    Log.d(TAG, "turn on");
                    model.getBeaconList();
                } else {
                    Log.d(TAG, "turn off");
                    displayToastMessageOnView(context.getString(R.string.ErrorMsg_TurnOffBleAndLoc));
                }
            }
        } else {
            displayToastMessageOnView(context.getString(R.string.ErrorMag_EnableLocAccess));
        }
    }

    public void displayToastMessageOnView(String msg){
        view.displayToastMessage(msg);
    }

    public interface View {
        void displayList(ArrayList<BeaconsInfoDBModel> getmData);
        void intentToOtherWebsite(Intent intent);
        void displayToastMessage(String msg);
    }
}
