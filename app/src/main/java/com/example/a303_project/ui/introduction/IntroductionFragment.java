package com.example.a303_project.ui.introduction;


import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a303_project.R;

import java.util.ArrayList;


public class IntroductionFragment extends Fragment implements IntroductionPresenter.View {
    protected static final String TAG = "NotificationsFragment";

    //Presenter
    private IntroductionPresenter introductionPresenter;

    //Recycler View
    private RecyclerView recycler_view;
    private IntroductionRecyclerViewAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        //Setup Presenter
        introductionPresenter = new IntroductionPresenter(this, getContext());

        //Setup Recycler View
        recycler_view = root.findViewById(R.id.recycler_view_intro);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_view.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        //Start the function
        introductionPresenter.start();
        return root;
    }

    //Display Arraylist with Recycler View
    @Override
    public void displayList(ArrayList<BeaconsInfoDBModel> mData) {
        adapter = new IntroductionRecyclerViewAdapter(mData, introductionPresenter);
        recycler_view.setAdapter(adapter);
    }

    //Intent to out Website
    @Override
    public void intentToOtherWebsite(Intent intent) {
        startActivity(intent);
    }

    //Display Toast Message
    @Override
    public void displayToastMessage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }
}