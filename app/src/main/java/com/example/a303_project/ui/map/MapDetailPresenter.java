package com.example.a303_project.ui.map;

import android.content.Context;
import android.content.Intent;

import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

public class MapDetailPresenter {
    private final String TAG = "MapDetailPresenter";

    MapDetailPresenter.View view;
    MapDetailModel model;
    Context context;

    public MapDetailPresenter(View view, Context context) {
        this.view = view;
        this.context = context;
        model = new MapDetailModel(this);
    }

    @TestOnly
    public MapDetailPresenter(View view, MapDetailModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
    }

    public void start(String StartPointID, String DestinationID){
        if(StartPointID != null && DestinationID!= null){
            model.genFloorPlan(StartPointID, DestinationID);
        }else{
            view.displayToastMessage("Error! Please go back prev page.");
        }
    }

    public void displayToastMessage(String string) {
        view.displayToastMessage(string);
    }

    public void drawFloorPlan(MapDetailDBModel dbModel) {
        view.drawFloorPlan(dbModel);
    }

    public interface View {
        void displayToastMessage(String msg);
        void drawFloorPlan(MapDetailDBModel dbModel);
    }
}
