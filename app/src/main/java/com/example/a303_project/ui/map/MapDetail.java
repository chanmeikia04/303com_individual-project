package com.example.a303_project.ui.map;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.a303_project.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MapDetail extends AppCompatActivity implements MapDetailPresenter.View {

    private static final String TAG = "MapDetail";
    private MapDetailPresenter presenter;
    private ImageView ivT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ivT= findViewById(R.id.ivT);

        presenter = new MapDetailPresenter(this, this);
        presenter.start(getIntent().getStringExtra("StartPointID"), getIntent().getStringExtra("DestinationID"));

        Log.d(TAG, getIntent().getStringExtra("StartPointID"));
        Log.d(TAG, getIntent().getStringExtra("DestinationID"));

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                flag = true;
                break;
            default:
                flag = super.onOptionsItemSelected(item);
                break;
        }
        return flag;
    }

    @Override
    public void displayToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void drawFloorPlan(MapDetailDBModel dbModel) {
        Log.i(TAG, "draw" + dbModel.getFloorPlan());
        String url = dbModel.getFloorPlan();
        Bitmap bmp = getBitmapFromURL(url);
        try {
            Bitmap newbit = Bitmap.createBitmap(700, 700, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(newbit);

            Paint paint = new Paint();

            // (0,0) -> floorPlan
            canvas.drawBitmap(bmp, 0, 0, paint);
            paint.setColor(getResources().getColor(R.color.purple_700));
            paint.setTextSize(30);

            // text
            Paint paintCircle3 = new Paint();
            paintCircle3.setStrokeWidth(8);
            paintCircle3.setColor(Color.RED);
            canvas.drawCircle(100,650,15,paintCircle3);
            canvas.drawText("You are here" , 150 , 660, paint);

            Paint paintCircle4 = new Paint();
            paintCircle4.setStrokeWidth(8);
            paintCircle4.setColor(Color.BLUE);
            canvas.drawCircle(100,685,15,paintCircle4);
            canvas.drawText("Your destination" , 150 , 700 , paint);

            paint.setColor(Color.RED);
            paint.setStrokeWidth(10);

            //I am here Point
            Paint paintCircle = new Paint();
            paintCircle.setStrokeWidth(8);
            paintCircle.setColor(Color.RED);
            canvas.drawCircle(dbModel.getsX(),dbModel.getsY(),20,paintCircle);

            //Destination Point
            Paint paintCircle1 = new Paint();
            paintCircle1.setStrokeWidth(8);
            paintCircle1.setColor(Color.BLUE);
            canvas.drawCircle(dbModel.getdX(),dbModel.getdY(),20,paintCircle1);

            ivT.setImageBitmap(newbit);
        }catch (Exception e) {
            // Log exception
            Log.i("test", e.toString());
        }
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int w, int h)
    {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        float scaleWidth = ((float) w) / width;
        float scaleHeight = ((float) h) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        return resizedBitmap;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return resizeBitmap(myBitmap, 600, 600);
        } catch (IOException e) {
            // Log exception
            Log.i("YYYY", e.toString());
            return null;
        }
    }
}