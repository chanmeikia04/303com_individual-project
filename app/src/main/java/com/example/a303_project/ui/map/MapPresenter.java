package com.example.a303_project.ui.map;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.example.a303_project.R;
import com.example.a303_project.ui.Museum.MuseumDetailActivity;

import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

public class MapPresenter {
    private final String TAG = "MapPresenter";
    private static final int REQUEST_CODE_ACCESS_COARSE_LOCATION = 1;

    MapPresenter.View view;
    MapModel model;
    Context context;

    public MapPresenter(View view, Context context) {
        this.view = view;
        this.context = context;
        model = new MapModel(this);
    }

    @TestOnly
    public MapPresenter(View view, MapModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
    }

    public void start() {
        //BLE
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Location - GPS
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        //Check if GPS is authorized
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //Check if bluetooth is supported
            if (bluetoothAdapter == null) {
                view.displayToastMessage(context.getString(R.string.ErrorMsg_NotSupBluetooth));
            } else {
                //Check if GPS and Bluetooth is Turn on
                if ((gps || network) && bluetoothAdapter.isEnabled()) {
                    Log.d(TAG, "turn on");
                    model.getBeaconList();
//                    model.getDestinInfo();
                } else {
                    Log.d(TAG, "turn off");
                    displayToastMessageOnView(context.getString(R.string.ErrorMsg_TurnOffBleAndLoc));
                }
            }
        } else {
            displayToastMessageOnView(context.getString(R.string.ErrorMag_EnableLocAccess));
        }
    }

    public void displayToastMessageOnView(String msg){
        view.displayToastMessage(msg);
    }

    public void displayCurrentPosition(String id, String msg) {
        view.displayCurrentPosition(id, msg);
    }

    public void displayDestinationList(ArrayList<SpinnerData> destinationList) {
//        Log.i(TAG, destinationList.toString());
        view.displayDestinationList(destinationList);
    }

    public void checkLoc(Spinner spDestination, String currenyPoID) {
        if(spDestination != null && spDestination.getSelectedItem() !=null && currenyPoID != null) {
            if(((SpinnerData) spDestination.getSelectedItem()).getValue().equals(currenyPoID)){
                view.displayToastMessage("你已在目的地\nYou are already at the destination");
            }else {
                Intent intent = new Intent(context, MapDetail.class);
                intent.putExtra("StartPointID", currenyPoID);
                intent.putExtra("DestinationID", ((SpinnerData) spDestination.getSelectedItem()).getValue());
                view.intentDetail(intent);
            }
        }
    }

    public interface View {
        void displayToastMessage(String msg);
        void displayCurrentPosition(String id, String msg);
        void displayDestinationList(ArrayList<SpinnerData> destinationList);
        void intentDetail(Intent intent);
    }
}
