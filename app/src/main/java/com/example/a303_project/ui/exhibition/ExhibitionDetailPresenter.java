package com.example.a303_project.ui.exhibition;

import com.bumptech.glide.Glide;

public class ExhibitionDetailPresenter{
    ExhibitionDetailPresenter.View view;
    ExhibitionModel model;
    public ExhibitionDetailPresenter(View view ,ExhibitionModel model) {
        this.model = model;
        this.view = view;
    }

    public void displayData() {
        //Name
        view.setName_cn(model.getName_cn());
        view.setName_en(model.getName_en());

        //Date
        if(model.getDate()!=""){
            view.setDate(model.getDate());
        }else{
            view.hiddenDate();
        }

        //Type
        if(model.getSpecialExhibition()){
            view.setType("專題展覽\nSpecial Exhibition");
        }else{
            view.setType("常設展覽\nPermanent Exhibition");
        }

        //Introduction
        view.setIntro_cn(model.getIntro_cn());
        view.setIntro_en(model.getIntro_en());

        if(model.getPic_url()!="") {
            view.setPic(model.getPic_url());
//            Glide.with(viewHolder.itemView.getContext()).load(localDataSet.get(position).getPic_url()).into(viewHolder.getIvPic());
        }

        if(model.getWebsite_url()!=""){
            view.setWebsite(model.getWebsite_url());
        }
    }

    public interface View {
        void setName_cn(String name_cn);
        void setName_en(String name_en);
        void setDate(String s);
        void setType(String s);
        void setIntro_cn(String intro_cn);
        void setIntro_en(String intro_en);
        void setPic(String pic_url);
        void setWebsite(String website_url);
        void hiddenDate();
    }
}
