package com.example.a303_project.ui.exhibition;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ExhibitionModel implements Serializable {
    private final String TAG = "ExhibitionModel";
    private String museumId, exhibitionID;
    private String name_cn, name_en;
    private String date;
    private String intro_cn, intro_en;
    private String pic_url, website_url;
    private boolean isSpecialExhibition;

    public ExhibitionModel() {}

    public ExhibitionModel(String museumId, String exhibitionID, String name_cn, String name_en, String date, String intro_cn, String intro_en, String pic_url, String website_url, boolean isSpecialExhibition) {
        this.museumId = museumId;
        this.exhibitionID = exhibitionID;
        this.name_cn = name_cn;
        this.name_en = name_en;
        this.date = date;
        this.intro_cn = intro_cn;
        this.intro_en = intro_en;
        this.pic_url = pic_url;
        this.website_url = website_url;
        this.isSpecialExhibition = isSpecialExhibition;
    }

    public String getExhibitionID() { return exhibitionID; }
    public void setExhibitionID(String exhibitionID) { this.exhibitionID = exhibitionID;}

    public String getMuseumId() {
        return museumId;
    }
    public void setMuseumId(String museumId) {
        this.museumId = museumId;
    }

    public String getName_cn() {
        return name_cn;
    }
    public void setName_cn(String name_cn) {
        this.name_cn = name_cn;
    }

    public String getName_en() {
        return name_en;
    }
    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getIntro_cn() {
        return intro_cn;
    }
    public void setIntro_cn(String intro_cn) {
        this.intro_cn = intro_cn;
    }

    public String getIntro_en() {
        return intro_en;
    }
    public void setIntro_en(String intro_en) {
        this.intro_en = intro_en;
    }

    public boolean getSpecialExhibition() {
        return isSpecialExhibition;
    }
    public void setSpecialExhibition(boolean specialExhibition) { isSpecialExhibition = specialExhibition; }

    public String getPic_url() { return pic_url; }
    public void setPic_url(String pic_url) { this.pic_url = pic_url; }

    public String getWebsite_url() { return website_url; }
    public void setWebsite_url(String website_url) { this.website_url = website_url; }
}
